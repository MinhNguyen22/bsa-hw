package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemBuilder;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemBuilder;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		// TODO: asd
		DockedShip dockedShip = new DockedShip();
		dockedShip.setName(name);
		dockedShip.setShieldHP(shieldHP);
		dockedShip.setHullHP(hullHP);
		dockedShip.setPowergridOutput(powergridOutput);
		dockedShip.setCapacitorAmount(capacitorAmount);
		dockedShip.setCapacitorRechargeRate(capacitorRechargeRate);
		dockedShip.setSpeed(speed);
		dockedShip.setSize(size);
		return dockedShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: asd
		setCorrectPG(subsystem);
		setAttackSubsystem(subsystem);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: asd
		if (defenciveSubsystem != null)
			setDefenciveSubsystem(null);
		setCorrectPG(subsystem);
		setDefenciveSubsystem(subsystem);

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		// TODO:asd
		if (attackSubsystem == null && defenciveSubsystem == null)
			throw NotAllSubsystemsFitted.bothMissing();
		if (attackSubsystem == null)
			throw NotAllSubsystemsFitted.attackMissing();
		if (defenciveSubsystem == null)
			throw NotAllSubsystemsFitted.defenciveMissing();
		return new CombatReadyShip(this);
	}

	public AttackSubsystem getAttackSubsystem() {
		return attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return defenciveSubsystem;
	}

	public String getName() {
		return name;
	}

	public PositiveInteger getSpeed() {
		return speed;
	}

	public PositiveInteger getSize() {
		return size;
	}

	public void setAttackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	public void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public void setCorrectPG(Subsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null)
			return;
		if (attackSubsystem != null || defenciveSubsystem != null) {
			int subPG = subsystem.getPowerGridConsumption().value();
			int defPG = defenciveSubsystem == null ? 0 : defenciveSubsystem.getPowerGridConsumption().value();
			int attackPG = attackSubsystem == null ? 0 : attackSubsystem.getPowerGridConsumption().value();
			int shipPG = powergridOutput.value();
			if (shipPG != attackPG + defPG + subPG) {
				throw new InsufficientPowergridException(shipPG - attackPG - defPG - subPG);
			}
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	public void setPowergridOutput(PositiveInteger powergridOutput) {
		this.powergridOutput = powergridOutput;
	}

	public void setCapacitorAmount(PositiveInteger capacitorAmount) {
		this.capacitorAmount = capacitorAmount;
	}

	public void setCapacitorRechargeRate(PositiveInteger capacitorRechargeRate) {
		this.capacitorRechargeRate = capacitorRechargeRate;
	}

	public void setSpeed(PositiveInteger speed) {
		this.speed = speed;
	}

	public void setSize(PositiveInteger size) {
		this.size = size;
	}

}
