package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;

public final class CombatReadyShip implements CombatReadyVessel {

	DockedShip dockedShip;

	boolean flag;

	public CombatReadyShip(DockedShip dockedShip) {
		this.dockedShip = dockedShip;
	}

	@Override
	public void endTurn() {
		// TODO: asd
		flag = false;
	}

	@Override
	public void startTurn() {
		// TODO: asd
		flag = true;
	}

	@Override
	public String getName() {
		// TODO: asd
		String name = dockedShip.getName();
		return name;
	}

	@Override
	public PositiveInteger getSize() {
		// TODO: asd
		PositiveInteger size = dockedShip.getSize();
		return size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		// TODO: asd
		PositiveInteger speed = dockedShip.getSpeed();
		return speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		// TODO:asd
		AttackAction action = new AttackAction(PositiveInteger.of(dockedShip.getAttackSubsystem().attack(target).value()), dockedShip.getDefenciveSubsystem(), target, dockedShip.getAttackSubsystem());
		Optional<AttackAction> attackAction = Optional.of(action);
		return attackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		// TODO: asd
		return null;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		// TODO:asd
		return null;
	}

}
