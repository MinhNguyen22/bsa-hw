package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	private double damage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
												PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
												PositiveInteger baseDamage) throws IllegalArgumentException {
		// TODO: asd
		if (name.trim().isEmpty())
			throw new IllegalArgumentException("Name should be not null and not empty");

		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.setName(name);
		attackSubsystem.setPowergridRequirments(powergridRequirments);
		attackSubsystem.setCapacitorConsumption(capacitorConsumption);
		attackSubsystem.setOptimalSpeed(optimalSpeed);
		attackSubsystem.setOptimalSize(optimalSize);
		attackSubsystem.setBaseDamage(baseDamage);
		return attackSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO:asd
		return powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO:asd
		return capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		// TODO: asd
		double sizeReductionModifier = target.getSize().value() >= optimalSize.value() ? 1
				: (double) target.getSize().value() / optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= optimalSpeed.value() ? 1
				: (double) optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		damage = baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);
		return PositiveInteger.of((int) Math.round(damage));
	}

	@Override
	public String getName() {
		// TODO: asd
		return name;
	}

	public double getDamage() {
		return damage;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPowergridRequirments(PositiveInteger powergridRequirments) {
		this.powergridRequirments = powergridRequirments;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setOptimalSpeed(PositiveInteger optimalSpeed) {
		this.optimalSpeed = optimalSpeed;
	}

	public void setOptimalSize(PositiveInteger optimalSize) {
		this.optimalSize = optimalSize;
	}

	public void setBaseDamage(PositiveInteger baseDamage) {
		this.baseDamage = baseDamage;
	}

}
