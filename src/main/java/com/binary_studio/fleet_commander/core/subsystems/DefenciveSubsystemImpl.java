package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		// TODO: asd
		if (name.trim().isEmpty())
			throw new IllegalArgumentException("Name should be not null and not empty");
		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.setName(name);
		defenciveSubsystem.setPowergridConsumption(powergridConsumption);
		defenciveSubsystem.setCapacitorConsumption(capacitorConsumption);
		defenciveSubsystem.setImpactReductionPercent(impactReductionPercent);
		defenciveSubsystem.setShieldRegeneration(shieldRegeneration);
		defenciveSubsystem.setHullRegeneration(hullRegeneration);
		return defenciveSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO:asd
		return powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO:asd
		return capacitorConsumption;
	}

	@Override
	public String getName() {
		// TODO: asd
		return name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		// TODO:asd
		int damage = incomingDamage.damage.value();
		NamedEntity attacker = incomingDamage.attacker;
		NamedEntity target = incomingDamage.target;
		NamedEntity weapon = incomingDamage.weapon;
		double percents = (double) (100 - impactReductionPercent.value()) / 100;
		if (percents < 0.05)
			percents = 0.06;
		damage = (int) Math.round(damage * percents);
		if (damage < 1)
			damage = 1;
		PositiveInteger resultDamage = PositiveInteger.of(damage);
		AttackAction attackAction = new AttackAction(resultDamage, attacker, target, weapon);

		return attackAction;
	}

	@Override
	public RegenerateAction regenerate() {
		// TODO: asd
		RegenerateAction regenerateAction = new RegenerateAction(shieldRegeneration, hullRegeneration);
		return regenerateAction;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPowergridConsumption(PositiveInteger powergridConsumption) {
		this.powergridConsumption = powergridConsumption;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setImpactReductionPercent(PositiveInteger impactReductionPercent) {
		this.impactReductionPercent = impactReductionPercent;
	}

	public void setShieldRegeneration(PositiveInteger shieldRegeneration) {
		this.shieldRegeneration = shieldRegeneration;
	}

	public void setHullRegeneration(PositiveInteger hullRegeneration) {
		this.hullRegeneration = hullRegeneration;
	}

}
