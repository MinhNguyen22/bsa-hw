package com.binary_studio.dependency_detector;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		// TODO: Implement
		int dependenciesSize = libraries.dependencies.size();
		int count = 0;
		if (dependenciesSize < 2)
			return true;
		String[] dependency1;
		String[] dependency2;
		for (int i = 0; i < dependenciesSize; i++) {
			dependency1 = libraries.dependencies.get(i);

			for (int k = 0; k < dependenciesSize; k++) {
				dependency2 = libraries.dependencies.get(k);
				if (dependency1[0].equals(dependency2[1]) && dependency1[1].equals(dependency2[0])
						|| count == libraries.libraries.size())
					return false;

				if (dependency1[1].equals(dependency2[0])) {
					count++;
					if (count == libraries.libraries.size())
						return false;
					break;
				}
			}
		}
		return true;
	}

}
