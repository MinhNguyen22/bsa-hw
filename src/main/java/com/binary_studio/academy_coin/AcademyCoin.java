package com.binary_studio.academy_coin;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		int profit = 0;
		int minPrice = 0;
		int maxPrice = 0;
		var list = prices.collect(Collectors.toList());
		minPrice = list.get(0);
		for (int i = 0; i < list.size() - 1;) {
			while (i < list.size() - 1 && list.get(i) > list.get(i + 1)) {
				minPrice = list.get(i + 1);
				i++;
			}
			while (i < list.size() - 1 && list.get(i) < list.get(i + 1)) {
				maxPrice = list.get(i + 1);
				i++;
			}
			if (i > 0 && minPrice != 0 && maxPrice != 0)
				profit += maxPrice - minPrice;
			minPrice = 0;
			maxPrice = 0;
		}
		return profit;
	}

}
